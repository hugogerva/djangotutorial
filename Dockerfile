FROM archlinux/base

RUN pacman --noconfirm -Syyu \
    && pacman --noconfirm -S git base-devel python \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
 
WORKDIR djangotutorial

RUN  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py \
    && python get-pip.py \
    && pip install pew 

RUN git clone https://gitlab.com/hugogerva/djangotutorial.git 
RUN pew new blog -a .

RUN pip install -r requirements.txt 

CMD [ "uwsgi", "--ini",  "./uwsgi.ini" ]
